DiskSpd

DiskSpd是微软视窗、视窗服务器和云服务器基础设施工程团队的存储性能工具。请访问https://github.com/Microsoft/diskspd/wiki获取最新文档。

除了工具本身之外，该存储库还托管利用DiskSpd的测量框架。最初的例子是用于Windows Server 2016超融合存储空间直接工作的虚拟机机群。

该项目采用了微软开源行为准则。如需更多信息，请参阅行为准则常见问题解答，或联系opencode@microsoft.com，询问任何其他问题或意见。

放

“发布”页面包括预编译的二进制文件(ZIP)和最新版本的DiskSpd工具的源代码。可以从https://github . com/MicroSoft/DiskSpd/releases/download/v 2 . 0 . 21 a/disk SPD-2 . 0 . 21 a . zip下载disk SPD的最新更新。

有什么新消息？

DISKSPD

DISKSPD 2.0.21a 9/21/2018

-增加了对内存映射输入/输出的支持:

-新-Sm-选项，启用内存映射输入/输出

-新建-不<虚拟网络接口>-为内存映射输入/输出指定刷新选项的选项

-增加了为Windows (ETW)事件提供事件跟踪的支持

-包括一个Windows性能记录器(WPR)配置文件，以启用ETW跟踪

-将系统信息添加到结果分析器输出中

DISKSPD 2.0.20a 2/28/2018

-可能需要重新划分结果的变更:

-新的随机数发生器，可以显示可观察到的成本降低

-切换到512字节对齐的缓冲区，带有-Z选项，以提高性能

-新-0-选项，用于指定每个线程的未完成输入输出请求数

-新-锆-选项，用于写入缓冲区内容的每次输入输出随机化

- XML:添加新的-<线程目标>-元素以支持目标加权方案

-从IOPS数据中获取的增强统计数据

-增加了对使用内置XSD验证XML配置文件的支持

-增加了对处理原始卷的支持

-更新了中央处理器统计数据，适用于64核以上的系统

-更新了中央处理器统计的计算和准确性

-重新启用对ETW统计的支持

DISKSPD 2.0.18a 5/31/2016

-更新- /？-使用示例-Sh- v .已弃用-h

-修复GPT分区介质上卷的操作(:)

-将输入输出优先级提示修正为正确的堆栈对齐(如果不是8字节，将会失败)

-使用iB符号说明文本结果输出以2^n单位(KiB/MiB/GiB)表示

DISKSPD 2.0.17a 5/01/2016

--S--扩展为独立于OS/软件缓存控制直写。其中，这允许指定缓冲直写(-Sbw-)。

- XML:添加新的-<直写>-元素来指定直写

- XML: -<禁用缓存>-不再发出(尽管仍在解析)，支持-<直写>-和-<禁用缓存>

-文本输出:操作系统/软件缓存和直写状态现在分别记录(相邻行)

-延迟直方图现在在文本和XML输出中报告为9-9(十亿分之一)

-由于无法打开写入内容源文件而添加的错误消息(-Z<size >，< file>-)

虚拟机车队

虚拟机机队0.9 10/2017(次要)

- watch-cpu:现在提供总的标准化cpu效用(考虑到turbo/speedstep)

- sweep-cputarget:现在在csv中提供平均的CSV文件系统读/写延迟

虚拟机数量0.8 6/2017

- get-cluspc:添加中小型企业客户端/服务器和中小型企业直连(存储组中尚未默认)

- test-clusterhealth:刷新调试存储子系统输出的输出管道

- watch-cluster:如果所有子作业不再运行，请立即重新启动

-观察-中央处理器:中央处理器核心利用率分布的新可视化工具

虚拟机数量0.7 3/2017

-create/destroy-vmfleet & update-csv:不要依赖包含虚拟设备友好名称的CSV名称

- create-vmfleet:如果basevhd不可访问，则出现错误

- create-vmfleet:使用$using:语法简化呼叫传递

- create-vmfleet:更改vhdx布局以匹配每个虚拟机的单独目录的scvmm行为(对ReF MRV很重要)

- create-vmfleet:默认情况下使用1个虚拟机大小(1个1.75千兆字节内存)

- start-vmfleet:尝试启动“失败”的虚拟机，通常有效

- set-vmfleet:增加对-SizeSpec的支持，用于A/D/D2v1 & v2大小规格，以便于重新配置

- stop-vmfleet:传入完整的名称列表，以允许关机的最佳内部并行化

- sweep-cputarget:使用%处理器性能来重新调整利用率并考虑涡轮效应

- test-clusterhealth:支持清理转储/分类材料，以简化持续监控(假设它们已经收集完毕)。(

-test-cluster health:stor port无响应设备事件的附加分类输出

-测试-集群运行状况:关于中小型企业客户端连接事件的附加分类注释

-test-cluster health:Melanox CX3/CX4错误计数器的新测试，用于诊断结构问题(坏电缆/收发器/roce细节/等)。(

- get-log:所有hv/clustering/smb事件通道的新分类日志收集器

- get-cluspc:新的跨群集性能计数器收集器

-删除由run-demo-<>.ps1替换的run-<>.ps1脚本

-检查异常:使用平均采样延迟，找出集群中异常设备的实验方法

虚拟机机队0.6 7/18/2016

-中央处理器目标扫描:扫描脚本使用存储操作系统和线性中央处理器/IOPS模型，根据中央处理器构建IOPS的经验扫描，最初针对三个经典的小型IOPS混音(100r、90:10和70:30 4K)。包括一个分析脚本，为每个结果提供线性模型。

-更新扫描机制，允许对DISKSPD扫描参数和主机性能计数器捕获进行通用规范。

-安装-虚拟机在CSV/VD结构就位后自动放置(添加路径、创建目录、复制、暂停)

-添加非线性检测来分析-cputarget

- get-linfit现在是一个实用程序脚本(生成描述fits的对象)

-所有标志文件(暂停/执行/完成)被下推到控制\标志目录

-演示脚本再次工作，并自动填充虚拟机/节点计数

-监视群集优雅地处理关闭/恢复的节点

- update-csv现在处理作为另一个节点(节点1、节点10)的逻辑前缀的节点名称

源代码

DiskSpd的源代码托管在GitHub上:

[https://github.com/Microsoft/diskspd](https://github.com/Microsoft/diskspd)

可以使用以下链接报告磁盘Spd的任何问题:

[https://github.com/Microsoft/diskspd/issues](https://github.com/Microsoft/diskspd/issues)